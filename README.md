Code and graphs for the [xkcd 2048](https://xkcd.com/2048/) comic. 

xkcd, sometimes styled XKCD, is a webcomic created in 2005 by American author Randall Munroe.

"[xkcd 2048](https://xkcd.com/2048/) brought a grimace of recognition to statisticians everywhere," - [revodavid](https://twitter.com/revodavid)

Barry Rowlingson reproduced most of the charts from xkcd 2048 with a simple R function, and the xkcd ggplot2 theme.
